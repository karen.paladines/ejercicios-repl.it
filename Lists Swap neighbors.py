# Read a list of integers:
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
def swap(nums):
    for i in range(0, len(a) - 1, 2):
        a[i], a[i + 1] = a[i + 1], a[i]
    return a
swap(a)
print(a)