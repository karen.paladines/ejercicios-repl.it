# Read a list of integers:
# Print a value:
# print(a)
x = input()
lst = list(map(int, x.split()))
total = 1
for i in range(1, len(lst)):
  if lst[i-1] != lst[i]:
    total += 1
print(total)
