# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
m, n = [int(s) for s in input().split()]
matrix = [[int(k) for k in input().split()] for i in range(m)]
a, b =[int(s) for s in input().split()]
for t in range(m):
    matrix[t][b], matrix[t][a] = matrix[t][a], matrix[t][b]
    print(*matrix[t], sep =' ')